import React from 'react';
import { Dimensions } from 'react-native';
import sucesos from './src/components/news/sucesos';
import sucesosSearch from './src/components/news/sucesosSearch';
import detalles2 from './src/components/news/detalles2';
import estadisticas from './src/components/estadisticas/estadisticas';
import map from './src/components/maps/map';
import {  createBottomTabNavigator,  createStackNavigator, } from 'react-navigation';

const SucesosStack = createStackNavigator({
  Sucesos: sucesos,
  Map: map,
  Estadisticas: estadisticas,
  Details: detalles2,
  Search: sucesosSearch
});

const EstadisticasStack = createStackNavigator({
  Sucesos: sucesos,
  Map: map,
  Estadisticas: estadisticas,
  Details: detalles2,
  Search: sucesosSearch
});


const SearchStack = createStackNavigator({
  Sucesos: sucesos,
  Map: map,
  Estadisticas: estadisticas,
  Details: detalles2,
  Search: sucesosSearch
});

const RootStack = createBottomTabNavigator(
  {
    Sucesos: SucesosStack,
    Map: map,
    Estadisticas: EstadisticasStack,
    Search: SearchStack
  },
  {
    /* Other configuration remains unchanged */
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack style={{ width: Dimensions.get("window").width }} />; 

  }
  
}

