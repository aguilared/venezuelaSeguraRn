import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, FlatList } from 'react-native';
import { List, ListItem } from "react-native-elements";
import AppHeader from 'containers/AppHeader1';
import moment from 'moment';
import 'moment-timezone';

var idLocale = require('moment/locale/es'); 

export default class sucesos extends Component { 
  state = {
    data: [],
    data2: [],
    data3: [],
  }

  static navigationOptions = {
    headerTitle: <AppHeader 
                    headerText = "Estadisticas." 
                    style={{ height: 80 }}
                  />,
  };
  
  componentDidMount() {
    this.fetchData();  
  }

  fetchData = async () => {
    const response1 = await fetch('http://34.229.201.249:1337/api/sucesos/AcuAnoActual'); //total del municipio
    const response2 = await fetch('http://34.229.201.249:1337/api/sucesos/AcuAnoActualParro1'); //un solo objeto parroquia
    const response3 = await fetch('http://34.229.201.249:1337/api/sucesos/AcuAnoActualParro');  //varios objetos x parroquias
    const json = await response1.json();
    const json2 = await response2.json();
    const json3 = await response3.json();
    this.setState({ data: json.acumu_meses });
    this.setState({ data2: json2 });
    this.setState({ data3: json3.acu_mes });
     
    console.log(Object.keys(json3));  // ["first", "last"]
    //console.log('eale');
    //Object.keys(json3).forEach(key => console.log(key, json3[key]));
    var primerElemento = this.state.data3[0];
    //console.log('eale1');
    console.log('elemento del objeto:'+primerElemento);
  };
  
  _renderTitle(title) {
    return (
      <View style={{ flex: 1, alignItems: 'center', marginTop: 20 }}>
        <Text style={{ fontSize: 20 }}>{title}</Text>
      </View>
    )
  }

  render() {
    const date = new Date();
    const titulo = "Homicidios Ciudad Guayana al: " + moment(date).format("LLL");
    const titulo1 = "Por Parroquias este Año";
    const titulo2 = "Por Parroquias este Mes";
    const { navigate } = this.props.navigation;   
    
    return (
      
      <View style={styles.container}>
        <ScrollView> 
          <Text style={styles.title}>{titulo}</Text>
          <List containerStyle={{ marginBottom: 20 }}>
            <FlatList data={this.state.data}
              keyExtractor={(x, i) => i} //x = item ..   
              renderItem={({ item }) =>
                <ListItem
                  roundAvatar
                  onPress={() => navigate('news', {
                  })}
                  subtitle={
                    <View style={styles.subtitleView}>
                      <Text style={styles.text}>Total en el Año: {item.totalAcuAno}, este Mes: {item.tot_mes}</Text>
                      <Text style={styles.text}>Enero: {item.ene}, Febrero: {item.feb}, Marzo: {item.mar}, </Text>
                      <Text style={styles.text}>Abril: {item.abr}, Mayo: {item.may}, Junio: {item.jun}</Text>
                      <Text style={styles.text}>Julio: {item.jul}, Agosto: {item.ago}, Septiembre: {item.sep}</Text>
                    </View>
                  }
                />
              }
            /> 
            
            <FlatList data={this.state.data2}
              keyExtractor={(x, i) => i} //x = item ..   
              renderItem={({ item }) =>
                <ListItem
                  roundAvatar
                  onPress={() => navigate('news', {
                  })}
                  subtitle={
                    <View style={styles.subtitleView}>
                      <Text style={styles.text}>Por Parroquias este AÑO: {item.total} </Text>
                      <Text style={styles.text}>Cachamay: {item.ano_parr_cacha}, Chirica: {item.ano_parr_chi}, </Text>
                      <Text style={styles.text}>Dalla Costa: {item.ano_parr_dalla}, Once Abril: {item.ano_parr_once}, </Text>
                      <Text style={styles.text}>Pozo Verde: {item.ano_parr_pozo}, Simon Bolvar: {item.ano_parr_simon}, </Text>
                      <Text style={styles.text1}>Unare: {item.ano_parr_unare}, Universidad: {item.ano_parr_uni}, </Text>
                      <Text style={styles.text}>Vistalsol: {item.ano_parr_vista}, Yocoima: {item.ano_parr_yoco}, </Text>
                    </View>
                  }
                />
              }
            />

            <FlatList data={this.state.data3}
              keyExtractor={(x, i) => i} //x = item ..   
              renderItem={({ item }) =>
                <ListItem
                  roundAvatar
                  onPress={() => navigate('news', {
                  })}
                  subtitle={
                    <View style={styles.subtitleView}>
                      <Text style={styles.text}>Por Parroquias este MES: {item.totalMes} </Text>
                      <Text style={styles.text}>Cachamay: {item.mes_parr_cacha}, Chirica: {item.mes_parr_chi}, </Text>
                      <Text style={styles.text}>Dalla Costa: {item.mes_parr_dalla}, Once Abril: {item.mes_parr_once}, </Text>
                      <Text style={styles.text}>Pozo Verde: {item.mes_parr_pozo}, Simon Bolvar: {item.mes_parr_simon}, </Text>
                      <Text style={styles.text1}>Unare: {item.mes_parr_unare}, Universidad: {item.mes_parr_uni}, </Text>
                      <Text style={styles.text}>Vistalsol: {item.mes_parr_vista}, Yocoima: {item.mes_parr_yoco}, </Text>
                    </View>
                  }
                />
              }
            />
            
          </List>
        </ScrollView> 
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 3,
    backgroundColor: '#FF0312',
  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: 'white',
  },
  subtitleView: {
    //flexDirection: 'row',
    paddingLeft: 10,
    paddingBottom: 5,
    paddingTop: 5,
  },
  text: {
    fontSize: 16,
    paddingTop: 5
    //fontFamily: 'Avenir',
  },
  text1: {
    fontSize: 16,
    color: 'tomato',
    //fontFamily: 'Avenir',

  },
}); 