import React, { Component } from 'react';
import { Linking, View, Text, Image, ScrollView, TouchableHighlight } from 'react-native';
import { Card, CardItem, Thumbnail, Body, Left } from 'native-base';
import AppHeader from 'containers/AppHeader1';
import moment from 'moment';
import 'moment-timezone';
import styles from './styles';
 
class detalles2 extends Component {
    
  static navigationOptions = {
    headerTitle: <AppHeader 
                    headerText = "Suceso Detallado." 
                    style={{ height: 80 }}
                  />,
  };
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const { params } = this.props.navigation.state;
    const fecha_suceso = params ? params.fecha_suceso : null;
    const titulo = params ? params.titulo : null;
    const fuente = params ? params.fuente : null;
    const nombre_victima = params ? params.nombre_victima : null;
    const edad = params ? params.edad : null;
    const sexo = params ? params.sexo : null;
    const municipio = params ? params.municipio : null;
    const parroquia = params ? params.parroquia : null;
    const mi_resena = params ? params.mi_resena : null;
    const uri = params ? params.uri : null;
    //console.log(JSON.stringify({ params }));
    return (
      
      <View style={styles.container}>
        <ScrollView>  
          <Card onPress={() => { Linking.openURL(`${fuente}`) }}>
            <CardItem>
              <Left>
                <Thumbnail source={require('assets/me.png')} />
                <Body>
                  <Text>{`${moment(fecha_suceso).format("LLL")}`}</Text>
                  <Text note>{`${titulo}`}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem button onPress={() => { Linking.openURL(`${fuente}`) }}>
              <Image source={{ uri }} style={{ height: 200, width: null, flex: 1 }} onPress={() => { Linking.openURL(`${fuente}`) }}/>
            </CardItem>
            <CardItem style={{ height: 10 }}>
              <Text>Municipio: {municipio} Parroquia: {parroquia}</Text>
            </CardItem>
            <CardItem style={{ height: 10 }}>
              <Text>Victima: {nombre_victima} Edad: {edad} Sexo: {sexo}</Text>
            </CardItem>
            <CardItem>
              <Body>
                  <Text style={{ fontWeight: "900" }}>{mi_resena}</Text>
              </Body>
            </CardItem>
            <TouchableHighlight
              onPress={() => { Linking.openURL(`${fuente}`) }}
              style={[styles.button, { backgroundColor: 'red' }]}>
              <Text style={styles.buttonText}>Go To Source </Text>
            </TouchableHighlight>
          </Card>
          
        </ScrollView> 
      </View>
      
    );
    
  }
}


export default detalles2;