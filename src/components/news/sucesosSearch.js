import React, { Component } from 'react';
import { View, Text, Image, FlatList } from 'react-native';
import { List, ListItem} from "react-native-elements";
import AppHeader from 'containers/AppHeader';
import SearchBar1 from './SearchBar1';
import moment from 'moment';
import 'moment-timezone';
import styles from './styles';

export default class sucesos extends Component { 
  
  state = {
    data: [],
  }

  constructor(props){
    super(props);

    this.onPressSearch = this.onPressSearch.bind(this);

  }
  static navigationOptions = {
    headerTitle: <AppHeader
      headerText="Search"
      style={{ height: 80 }}
    />,
  };

  componentDidMount() {
    this.onPressSearch();  
  }

  async onPressSearch(search){

  //console.log(`http://34.229.201.249:1337/api/sucesos/findall8?search=${search ? search : 'topo'}`);
  let response = await fetch(`http://34.229.201.249:1337/api/sucesos/findall8?search=${search ? search : ''}`);
    console.log('antes' + response);
    response = await response.json();
    console.log('luego'+response);
    this.setState({ data: response });
  
  };

  _renderTitle(title) {  
    return (
      <View style={{ flex: 1, alignItems: 'center', marginTop: 20 }}>
        <Text style={{ fontSize: 20 }}>{title}</Text>
      </View>
    )
  }

  render() {
    const date = new Date();
    const titulo = "Ultimos Sucesos al: " + moment(date).format("LLL");
    const { navigate } = this.props.navigation;   
    
    return (
      
      <View style={styles.container}>  
        
        <Text style={styles.title}>{titulo}</Text>
        <SearchBar1 
          onPressSearch={this.onPressSearch}
        />
               
        <List containerStyle={{ marginBottom: 20 }}>
          <FlatList data={this.state.data}
            keyExtractor={(x, i) => i} //x = item ..   
            renderItem={({ item }) =>
              <ListItem  
                roundAvatar
                onPress={() => navigate('detalles2', {
                  fecha_suceso: item.fecha_suceso, id_suceso: item.suceso_id, titulo: item.titulo, detalle_delito: item.detalle_delito,
                  municipio: item.municipio, parroquia: item.parroquia, nombre_victima: item.nombre_victima, edad: item.edad, sexo: item.sexo,
                  sector: item.sector, mi_resena: item.mi_resena, fuente: item.fuente, uri: item.url_img })} 
                title={`${moment(item.fecha_suceso).format("LLL")}`} 
                subtitle={
                  <View style={styles.subtitleView}> 
                    <Image
                      source={{
                        uri: item.url_img,
                        cache: 'only-if-cached',
                      }}
                      style={{ width: 250, height: 100 }}
                    />
                    <Text style={styles.ratingText}>{`${item.titulo}`}</Text>
                  </View>
                }
              />
            }
          /> 
        </List>
        
      </View>
    
      
    );
  }
}

