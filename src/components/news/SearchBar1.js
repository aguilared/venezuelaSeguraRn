import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import { Button } from 'react-native-elements';

class SearchBar1 extends Component {
  state = { search: '' };

  render() {
    const {
      containerStyle,
      searchTextStyle,
      buttonStyle
    } = styles;

    return (
      <View style={containerStyle}>
        <TextInput
          style={searchTextStyle}
          underlineColorAndroid='transparent'
          placeholder='Search'
          onChangeText={search => this.setState({ search })}
          value={this.state.search}
          autoFocus={true}
        />
        <Button
          buttonStyle={buttonStyle}
          icon={{ name: 'search', size: 24 }}
          title={this.props.loading ? 'Searching...' : 'Search'}
          onPress={() => this.props.onPressSearch(this.state.search)}
        />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  searchTextStyle: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#BE3A31'
  },
  buttonStyle: {
    height: 30,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: '#BE3A31'
  }
};


export default SearchBar1;