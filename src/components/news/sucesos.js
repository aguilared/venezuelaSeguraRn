import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList } from 'react-native';
import { List, ListItem } from "react-native-elements";
import AppHeader from 'containers/AppHeader';
import moment from 'moment';
import 'moment-timezone';

export default class sucesos extends Component { 
  state = {
    data: [],
    data2: [],
    tmes: 0,
    totalAcuAno: 0,
  }

  static navigationOptions = {
    headerTitle: <AppHeader 
                    headerText = "Venezuela Segura - Bolivar." 
                    style={{ height: 80 }}
                  />,
    headerStyle: {
      backgroundColor: '#f4511e',
    },
  };
  
  componentDidMount() {
    this.fetchData();  
  }

  fetchData = async () => {
    //const response = await fetch('http://34.229.201.249:1337/api/sucesos/list3?page=0&per_page=8');
    //const response2 = await fetch('http://34.229.201.249:1337/api/sucesos/AcuAnoActual'); //total del municipio
    const response = await fetch("http://18.216.139.132:1337/api/v1/sucesos/list3?page=0&per_page=8");
    const response2 = await fetch("http://18.216.139.132:1337/api/v1/sucesos/AcuAnoActual"); //total del municipio
    const json = await response.json();
    const json2 = await response2.json();
    this.setState({ data: json });
    this.setState({ data2: json2.acumu_meses });
    console.log('json'+Object.keys(json)); // ["first", "last"]
    console.log('data2'+this.state.data2);

    this.setState({ tmes: this.state.data2[0].tot_mes });
    this.setState({ totalAcuAno: this.state.data2[0].totalAcuAno });
    
  };

  _renderTitle(title) {
    return (
      <View style={{ flex: 1, alignItems: 'center', marginTop: 20 }}>
        <Text style={{ fontSize: 20 }}>{title}</Text>
      </View>
    )
  }

  render() {
    const date = new Date();
    const titulo = "Ultimos Sucesos al: " + moment(date).format("LLL") + ", Homicidios Ciudad Guayana este mes: " + this.state.tmes + ", año= " + this.state.totalAcuAno ;
    const { navigate } = this.props.navigation;   
    
    return (
      
      <View style={styles.container}>  
        
        <Text style={styles.title}>{titulo}</Text>
        <List containerStyle={{ marginBottom: 20 }}>
          <FlatList data={this.state.data}
            keyExtractor={(x, i) => i} //x = item ..   
            renderItem={({ item }) =>
              <ListItem  
                roundAvatar
                onPress={() => this.props.navigation.navigate('Details', {
                  fecha_suceso: item.fecha_suceso, id_suceso: item.suceso_id, titulo: item.titulo, detalle_delito: item.detalle_delito,
                  municipio: item.municipio, parroquia: item.parroquia, nombre_victima: item.nombre_victima, edad: item.edad, sexo: item.sexo,
                  sector: item.sector, mi_resena: item.mi_resena, fuente: item.fuente, uri: item.url_img })} 
                title={`${moment(item.fecha_suceso).format("LLL")}`} 
                subtitle={
                  <View style={styles.subtitleView}> 
                    <Image
                      source={{
                        uri: item.url_img,
                        cache: 'only-if-cached',
                      }}
                      style={{ width: 250, height: 100 }}
                    />
                    <Text style={styles.ratingText}>{`${item.titulo}`}</Text>
                  </View>
                }
              />
            }
          /> 
        </List>
        
      </View>
       
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 3,
    backgroundColor: '#FF0312',
    
  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: 'white',
    margin: 3,
  },
  
}); 