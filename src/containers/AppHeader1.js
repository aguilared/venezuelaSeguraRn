import React from 'react';
import { Header } from "react-native-elements";
import { View } from 'react-native';

const AppHeader = ({ headerText}) => (
  <View>
    <Header
        centerComponent={{ text: headerText, style: { color: '#fff' } }}
        backgroundColor="red"
    />
  </View>
);

export default AppHeader;