import React from 'react';
import { Header } from "react-native-elements";
import { View, StyleSheet } from 'react-native';

const AppHeader = ({ headerText}) => (
  <View style={styles.container}>  
    <Header
        leftComponent={{ icon: 'menu', color: '#fff', onPress: () => navigate('News') }}
        centerComponent={{ text: headerText, style: { color: '#fff' } }}
        rightComponent={{ icon: 'home', color: '#fff', onPress: () => navigate('sucesos') }}
        backgroundColor="red"
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 3,
    backgroundColor: '#FF0312',

  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: 'white',
    margin: 3,
  },

}); 

export default AppHeader; 